<br/>
<p align="center">
  <a href="https://gitlab.com/sapcpro_sergij/Pong-Mobile">
    <img src="https://gitlab.com/sapcpro_sergij/Pong/-/raw/main/icon.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Pong Mobile</h3>

  <p align="center">
    The pocket version of Pong
    <br/>
    <br/>
    <a href="https://gitlab.com/sapcpro_sergij/Pong-Mobile/-/issues">Report Bug</a>
    .
    <a href="https://gitlab.com/sapcpro_sergij/Pong-Mobile/-/issues">Request Feature</a>
  </p>
</p>

![Releases](https://img.shields.io/gitlab/v/release/sapcpro_sergij/Pong-Mobile) ![Contributors](https://img.shields.io/gitlab/contributors/sapcpro_sergij/Pong-Mobile?color=dark-green) ![Forks](https://img.shields.io/gitlab/forks/sapcpro_sergij/Pong-Mobile?style=social) ![Stargazers](https://img.shields.io/gitlab/stars/sapcpro_sergij/Pong-Mobile?style=social) ![Issues](https://img.shields.io/gitlab/issues/open/sapcpro_sergij/Pong-Mobile) ![License](https://img.shields.io/gitlab/license/sapcpro_sergij/Pong-Mobile) 

## Table Of Contents

* [About the Project](#about-the-project)
* [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Contributing](#contributing)
* [Authors](#authors)
* [Acknowledgements](#acknowledgements)

## About The Project

Just like the headline says, the pocket version of Pong.

NOTES: I made this just for Learning purposes it's not complete!;
            You will have to build it from source to play it on Android

## Built With

This project is Built with Python and kivy

## Getting Started


### Prerequisites

To test it on PC:
         You have to have Python installed

To test it on Android or iOS phone, you will have to build it from source with buildozer
         You have to have Linux, or Linux virtual machine (Search on "Kivy Python script to APK")

### Installation

Clone the Repository and run the PongApp.py script to test it on PC

You will have to Build it from Source to test it on Android/iOS
Here's a guide: 
         https://buildozer.readthedocs.io/en/latest/quickstart.html

## Usage

Play the Game, or Examine the Code

## Authors

* **Sergij Aleksovski** - *A boy who Learns and is Programming* - [Sergij Aleksovski](https://gitlab.com/sapcpro_sergij/) -

## Acknowledgements

* [Sergij-SApcPro](https://gitlab.com/sapcpro_sergij/)
